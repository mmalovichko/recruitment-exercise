package com.sdl.exercise.model.domain.impl;

import com.sdl.exercise.model.domain.Skill;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Test for model - DefaultSkill.
 */
public class DefaultSkillTest {

    private Skill skill;
    private Skill nullSkill;

    @Before
    public void setUp() throws Exception {
        skill = new DefaultSkill("Java", Priority.HIGH);
        nullSkill = new DefaultSkill(null, null);
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("Java", skill.getName());
        assertNull(nullSkill.getName());
    }

    @Test
    public void testGetPriority() throws Exception {
        assertEquals(Priority.HIGH, skill.getPriority());
        assertNull(nullSkill.getPriority());
    }

    @Test
    public void testMatches() throws Exception {
        assertTrue(skill.matches("Java"));
        assertFalse(nullSkill.matches("Java"));
    }

}
