package com.sdl.exercise.context;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Spring Context configuration.
 */
@Configuration
@ComponentScan(basePackages = "com.sdl.exercise")
public class CoreConfiguration {
}
