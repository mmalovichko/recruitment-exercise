package com.sdl.exercise.dao;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.sdl.exercise.db.ListDataBase;
import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.Priority;

/**
 * Generic Dao.
 * @param <E> typed for Skill.
 */
public abstract class GenericDao<E extends Skill> implements Dao<E> {

    private final ListDataBase<E> listDataBase;

    protected GenericDao(ListDataBase<E> listDataBase) {
        this.listDataBase = listDataBase;
    }

    @Override
    public Iterable<E> listSkills() {
        return listDataBase.getList();
    }

    @Override
    public Iterable<E> listSkillsFor(final Priority priority) {
        return Iterables.filter(listDataBase.getList(), new Predicate<Skill>() {
            public boolean apply(Skill skill) {
                return skill.getPriority().getCode() == priority.getCode();
            }
        });
    }

    @Override
    public Iterable<E> listSkillsGreaterThanOrEqualTo(final Priority priority) {
        return Iterables.filter(listDataBase.getList(), new Predicate<Skill>() {
            public boolean apply(Skill skill) {
                return skill.getPriority().getCode() >= priority.getCode();
            }
        });
    }
}
