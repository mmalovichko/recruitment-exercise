package com.sdl.exercise.dao.impl;

import com.sdl.exercise.dao.GenericDao;
import com.sdl.exercise.db.ListDataBase;
import com.sdl.exercise.model.domain.Skill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Implementation of SkillDao.
 */
@Repository
public class SkillDao extends GenericDao<Skill> {

    @Autowired
    public SkillDao(ListDataBase<Skill> listDataBase) {
        super(listDataBase);
    }
}
