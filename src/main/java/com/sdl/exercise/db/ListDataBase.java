package com.sdl.exercise.db;

import com.sdl.exercise.model.domain.Skill;

import java.util.List;

/**
 * Contract for all simple list DB.
 * @param <E> any sub-class of Skill.
 */
public interface ListDataBase<E extends Skill> {
    List<E> getList();

}
