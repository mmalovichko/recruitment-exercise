package com.sdl.exercise.db.impl;

import com.sdl.exercise.db.ListDataBase;
import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.DefaultSkill;
import com.sdl.exercise.model.domain.impl.HierarchicalSkill;
import com.sdl.exercise.model.domain.impl.Priority;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ArrayList implementation of DataBase.
 */
@Component
public class ArrayListDataBase implements ListDataBase<Skill> {

    private List<Skill> skills;

    @PostConstruct
    public void initDb() {

        List<Skill> mutableSkills = new ArrayList<>();
        mutableSkills.add(new DefaultSkill("Spring", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Maven", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Git", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Mockito", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Scala", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Perl", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Fortran", Priority.LOW));

        Skill java = new DefaultSkill("Java", Priority.HIGH);
        Skill java8 = new HierarchicalSkill(java, "Java 8", Priority.HIGH);
        mutableSkills.add(java);
        mutableSkills.add(java8);

        this.skills = Collections.unmodifiableList(mutableSkills);

    }

    @Override
    public List<Skill> getList() {
        return skills;
    }
}
