package com.sdl.exercise.model.domain;


import com.sdl.exercise.model.domain.impl.Priority;

/**
 * Contract for all skills.
 */
public interface Skill {
    String getName();
    Priority getPriority();
    boolean matches(String skillName);
}
