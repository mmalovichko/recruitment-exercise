package com.sdl.exercise.model.domain.impl;

/**
 * List of priorities.
 */
public enum Priority {
    /**
     * High priority.
     */
    HIGH(3),
    /**
     * Medium priority.
     */
    MEDIUM(2),
    /**
     * Low priority.
     */
    LOW(1);

    private int code;

    Priority(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
