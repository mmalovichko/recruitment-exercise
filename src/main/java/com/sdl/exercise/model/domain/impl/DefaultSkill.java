package com.sdl.exercise.model.domain.impl;

import com.google.common.base.Objects;
import com.sdl.exercise.model.domain.Skill;
import org.springframework.core.style.ToStringCreator;


/**
 * Default skill.
 */
public class DefaultSkill implements Skill {

    protected static final int ODD_PRIME = 31;

    private String name;
    private Priority priority;

    public DefaultSkill(String s, Priority p) {
        this.name = s;
        this.priority = p;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public boolean matches(String skillName) {
        return Objects.equal(skillName, name);
    }

    @Override
    public String toString() {
        return new ToStringCreator(this).append("name", name).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DefaultSkill that = (DefaultSkill) o;

        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (priority != that.priority) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = ODD_PRIME * result + (priority != null ? priority.hashCode() : 0);
        return result;
    }
}
