package com.sdl.exercise.model.domain.impl;

import com.sdl.exercise.model.domain.Skill;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for model - HierarchicalSkill.
 */
public class HierarchicalSkillTest {

    private HierarchicalSkill hierarchicalSkill;

    @Before
    public void setUp() throws Exception {
        Skill parentSkill = new DefaultSkill("Spring", Priority.LOW);
        hierarchicalSkill = new HierarchicalSkill(parentSkill, "Java 8", Priority.HIGH);
    }

    @Test
    public void testMatches() throws Exception {
        assertFalse(hierarchicalSkill.matches("Erlang"));
        assertTrue(hierarchicalSkill.matches("Java 8"));
        assertTrue(hierarchicalSkill.matches("Spring"));
    }

    @Test
    public void testMatchesNullEmpty() throws Exception {
        assertFalse(hierarchicalSkill.matches(""));
        assertFalse(hierarchicalSkill.matches(null));
    }
}
