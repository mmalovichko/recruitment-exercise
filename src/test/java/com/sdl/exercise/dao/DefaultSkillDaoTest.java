package com.sdl.exercise.dao;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.sdl.exercise.context.TestCoreConfiguration;
import com.sdl.exercise.db.impl.ArrayListDataBase;
import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.DefaultSkill;
import com.sdl.exercise.model.domain.impl.HierarchicalSkill;
import com.sdl.exercise.model.domain.impl.Priority;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;


/**
 * Test dao layer.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestCoreConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class DefaultSkillDaoTest {

    private static final int EXPECTED_LOW_SKILLS = 2;
    private static final int EXPECTED_MEDIUM_SKILLS = 3;
    private static final int EXPECTED_HIGH_SKILLS = 5;
    private static final int EXPECTED_MEDIUM_OR_HIGHER_SKILLS = 8;

    @InjectMocks
    @Autowired
    private Dao<Skill> dao;

    @Mock
    private ArrayListDataBase db;

    private Skill goodSkill;
    private Skill badSkill;
    private List<Skill> expectedSkills;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        List<Skill> mutableSkills = new ArrayList<>();
        mutableSkills.add(new DefaultSkill("Spring", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Groovy", Priority.LOW));
        mutableSkills.add(new DefaultSkill("Maven", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Git", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Mockito", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Scala", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Perl", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Fortran", Priority.LOW));

        Skill java = new DefaultSkill("Java", Priority.HIGH);
        Skill java8 = new HierarchicalSkill(java, "Java 8", Priority.HIGH);
        mutableSkills.add(java);
        mutableSkills.add(java8);

        this.expectedSkills = Collections.unmodifiableList(mutableSkills);

        goodSkill = new DefaultSkill("Perl", Priority.MEDIUM);
        badSkill = new DefaultSkill("Ballet", Priority.LOW);
    }

    @Test
    public void testListSkills() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);

        Iterable<Skill> skills = dao.listSkills();

        assertNotNull(Iterables.find(skills, new Predicate<Skill>() {
            @Override
            public boolean apply(@Nullable Skill skill) {
                return goodSkill.equals(skill);
            }
        }));
    }


    @Test(expected = NoSuchElementException.class)
    public void testBadListSkills() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkills();
        assertNull(Iterables.find(skills, new Predicate<Skill>() {
            @Override
            public boolean apply(@Nullable Skill skill) {
                return badSkill.equals(skill);
            }
        }));
    }

    @Test
    public void testListSkillsForLow() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsFor(Priority.LOW);
        assertEquals("Number of skills greater than or equal to LOW not correct",
                EXPECTED_LOW_SKILLS, Iterables.size(skills));
    }

    @Test
    public void testListSkillsForMedium() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsFor(Priority.MEDIUM);
        assertEquals("Number of skills greater than or equal to MEDIUM not correct",
                EXPECTED_MEDIUM_SKILLS, Iterables.size(skills));
    }

    @Test
    public void testListSkillsForHigh() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsFor(Priority.HIGH);
        assertEquals("Number of skills greater than or equal to HIGH not correct",
                EXPECTED_HIGH_SKILLS, Iterables.size(skills));
    }

    @Test
    public void testListSkillsGreaterThanOrEqualToPriority() {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsGreaterThanOrEqualTo(Priority.MEDIUM);
        assertEquals("Number of skills greater than or equal to MEDIUM not correct",
                EXPECTED_MEDIUM_OR_HIGHER_SKILLS, Iterables.size(skills));
    }


    @Test(expected = NullPointerException.class)
    public void testListSkillsGreaterThanOrEqualToPriorityNull() {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsGreaterThanOrEqualTo(null);
        assertEquals("Number of skills greater than or equal to MEDIUM not correct",
                EXPECTED_LOW_SKILLS, Iterables.size(skills));
    }

    @Test(expected = NullPointerException.class)
    public void testListSkillsForNull() throws Exception {
        when(db.getList()).thenReturn(expectedSkills);
        Iterable<Skill> skills = dao.listSkillsFor(null);
        assertEquals("Number of skills greater than or equal to HIGH not correct",
                EXPECTED_LOW_SKILLS, Iterables.size(skills));
    }
}
