package com.sdl.exercise.model.domain.impl;

import com.sdl.exercise.model.domain.Skill;

/**
 * Hierarchical Skill.
 */
public class HierarchicalSkill extends DefaultSkill {

    private Skill parent;

    public HierarchicalSkill(Skill parent, String name,  Priority priority) {
        super(name, priority);
        this.parent = parent;
    }

    @Override
    public boolean matches(String skillName) {
        return parent.matches(skillName) || super.matches(skillName);
    }

    public Skill getParent() {
        return parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        HierarchicalSkill that = (HierarchicalSkill) o;

        if (parent != null ? !parent.equals(that.parent) : that.parent != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = ODD_PRIME * result + (parent != null ? parent.hashCode() : 0);
        return result;
    }
}
