# Recruitment Exercise

This project has been created with a lot of mistakes and bad practices.   Please can you correct and improve on this
project.  Pay particular attention to the acceptance criteria below.

If anything:

* does not satisfy acceptance criteria
* impacts support and maintenance of the module
* risks performance

then please correct.   Leave the project in a state that you would you be happy for another colleague to use.

All changes MUST be committed into the local git repository - i.e. in the directory that this readme is located.
When you have finished your work please package up this directory and send back to SDL.  Please also state how
long you have worked on this task.

# Project Overview

You have been asked to develop a system that retrieves a list of available skills and their relative priorities in
various different ways. The system currently provides a predefined list of technical skills together with their priorities.

## Story

* In order to know what skills are available for use
* As an API user
* I want to get a list of available skills

## Acceptance criteria

### All

* Given the skills service
* When I call the listSkills() API
* Then I get the list of skills that I can use

### Match

* Given the skills service
* When I call the listSkillsFor(Priority.HIGH) API
* Then I get the list of skills that are high priority

### Greater Than Or Equals

* Given the skills service
* When I call the listSkillsGreaterThanOrEqualTo(Priority.HIGH) API
* Then I get the list of skills that are either equal to the given priority or higher than the given priority.

### Hierarchical skill

* Given a skill "Y" has a parent skill name "X" (i.e. is a HierarchicalSkill)
* When I call matches("X") or matches("x") (case insensitive match) on skill "Y"
* Then the method returns true

### Random Ordered List

NEW : No implementation has taken place for the following acceptance criteria.  Please update the implementation
so that these acceptance criteria are met.

* Given the skill service
* When I call the listRandomSkills(int max) where max is less than the number of skills available
* Then I get back a random ordered list with the number of skills as specified in the max argument

* Given the skill service
* When I call the listRandomSkills(int max) where max is greater than or equal to the number of skills available
* Then I get back a random ordered list of ALL the available skills.

* Given the skill service
* When I call the listRandomSkills()
* Then I get back a random ordered list of ALL the available skills.
