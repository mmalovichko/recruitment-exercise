package com.sdl.exercise.service.impl;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sdl.exercise.dao.Dao;
import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.HierarchicalSkill;
import com.sdl.exercise.model.domain.impl.Priority;
import com.sdl.exercise.service.SkillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Default skill service - simple wrapper for SkillDao.
 */
@Service
public class DefaultSkillService implements SkillService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkillService.class);

    @Autowired
    private Dao<Skill> dao;


    @Override
    public Iterable<Skill> listSkills() {
        return dao.listSkills();
    }

    @Override
    public Iterable<Skill> listSkillsFor(Priority priority) {
        LOGGER.info("listSkillsFor priority = {}", priority);
        return dao.listSkillsFor(priority);
    }

    @Override
    public Iterable<Skill> listSkillsGreaterThanOrEqualTo(Priority priority) {
        LOGGER.info("listSkillsGreaterThanOrEqualTo priority = {}", priority);
        return dao.listSkillsGreaterThanOrEqualTo(priority);
    }

    @Override
    public boolean hasSkill(Iterable<Skill> skills, String skillName) {
        LOGGER.info("hasSkill skillName = {}", skillName);

        for (Skill skill : skills) {
            if (skill instanceof HierarchicalSkill) {
                if (((HierarchicalSkill) skill).getParent().matches(skillName)) {
                    return true;
                }
            } else {
                if (skill.matches(skillName)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Iterable<Skill> listRandomSkills(int max) {
        if (max <= 0) {
            throw new RuntimeException("max cannot be 0 or less");
        }

        Iterable<Skill> allSkills = listSkills();
        int size = Iterables.size(allSkills);

        if (max >= size) {
            return listRandomSkills();
        }

        List<Skill> listSkills = Lists.newArrayList(allSkills);
        List<Skill> newSkills = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            newSkills.add(listSkills.get(new Random().nextInt(max)));
        }

        return newSkills;
    }

    @Override
    public Iterable<Skill> listRandomSkills() {
        Iterable<Skill> allSkills = listSkills();
        ArrayList<Skill> listSkills = Lists.newArrayList(allSkills);
        Collections.shuffle(listSkills);
        return listSkills;
    }

}
