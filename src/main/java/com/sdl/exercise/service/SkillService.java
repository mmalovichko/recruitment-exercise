package com.sdl.exercise.service;

import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.Priority;

/**
 * Contract for wrapper of Skill dao.
 */
public interface SkillService {

    Iterable<Skill> listSkills();

    Iterable<Skill> listSkillsFor(Priority priority);

    Iterable<Skill> listSkillsGreaterThanOrEqualTo(Priority priority);

    boolean hasSkill(Iterable<Skill> skills, String skillName);

    Iterable<Skill> listRandomSkills(int max);

    Iterable<Skill> listRandomSkills();
}
