package com.sdl.exercise.service.impl;

import com.google.common.collect.Iterables;
import com.sdl.exercise.context.TestCoreConfiguration;
import com.sdl.exercise.dao.Dao;
import com.sdl.exercise.model.domain.Skill;
import com.sdl.exercise.model.domain.impl.DefaultSkill;
import com.sdl.exercise.model.domain.impl.HierarchicalSkill;
import com.sdl.exercise.model.domain.impl.Priority;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for SkillDao wrapper - DefaultSkillService. Cover additional method.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestCoreConfiguration.class, loader = AnnotationConfigContextLoader.class)
public class DefaultSkillServiceTest {

    private static final int METHOD_INVOKE_NUMBER = 4;
    private static final int TOTAL_SKILL_NUMBER = 10;
    private static final int LESS_THEN_SKILL_NUMBER = 8;
    private static final int MORE_THEN_SKILL_NUMBER = 12;

    @Autowired
    @InjectMocks
    private DefaultSkillService skillService;

    @Mock
    private Dao<Skill> dao;

    private List<Skill> expectedSkills;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        List<Skill> mutableSkills = new ArrayList<>();
        mutableSkills.add(new DefaultSkill("Spring", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Groovy", Priority.LOW));
        mutableSkills.add(new DefaultSkill("Maven", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Git", Priority.HIGH));
        mutableSkills.add(new DefaultSkill("Mockito", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Scala", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Perl", Priority.MEDIUM));
        mutableSkills.add(new DefaultSkill("Fortran", Priority.LOW));

        Skill java = new DefaultSkill("Java", Priority.HIGH);
        Skill java8 = new HierarchicalSkill(java, "Java 8", Priority.HIGH);
        mutableSkills.add(java);
        mutableSkills.add(java8);

        this.expectedSkills = Collections.unmodifiableList(mutableSkills);
    }


    @Test
    public void testHasSkill() throws Exception {
        when(dao.listSkills()).thenReturn(expectedSkills);
        assertFalse(skillService.hasSkill(skillService.listSkills(), "Name"));
        assertTrue(skillService.hasSkill(skillService.listSkills(), "Spring"));
        assertTrue(skillService.hasSkill(skillService.listSkills(), "Mockito"));
        assertTrue(skillService.hasSkill(skillService.listSkills(), "Groovy"));
        verify(dao, times(METHOD_INVOKE_NUMBER)).listSkills();
    }


    @Test
    public void testListRandomSkillsGreaterThenMax() throws Exception {
        when(dao.listSkills()).thenReturn(expectedSkills);
        Iterable<Skill> randomSkills = skillService.listRandomSkills(MORE_THEN_SKILL_NUMBER);
        assertEquals(TOTAL_SKILL_NUMBER, Iterables.size(randomSkills));

        Iterable<Skill> notRandomSkills = skillService.listSkills();
        assertTrue(isEqualOrder(randomSkills, notRandomSkills));
    }

    @Test
    public void testListRandomSkills() throws Exception {
        when(dao.listSkills()).thenReturn(expectedSkills);
        Iterable<Skill> notRandomSkills = skillService.listSkills();
        Iterable<Skill> randomSkills = skillService.listRandomSkills();

        assertEquals(TOTAL_SKILL_NUMBER, Iterables.size(randomSkills));
        assertTrue(isEqualOrder(randomSkills, notRandomSkills));

        Iterable<Skill> anotherRandomSkills = skillService.listRandomSkills();
        assertEquals(TOTAL_SKILL_NUMBER, Iterables.size(anotherRandomSkills));
        assertTrue(isEqualOrder(randomSkills, anotherRandomSkills));

    }


    @Test
    public void testListRandomSkillsMax() throws Exception {
        when(dao.listSkills()).thenReturn(expectedSkills);
        Iterable<Skill> skills = skillService.listRandomSkills(LESS_THEN_SKILL_NUMBER);
        assertEquals(LESS_THEN_SKILL_NUMBER, Iterables.size(skills));
    }


    @Test(expected = RuntimeException.class)
    public void testListRandomSkillsMaxWrongArg() throws Exception {
        when(dao.listSkills()).thenReturn(expectedSkills);
        Iterable<Skill> skills = skillService.listRandomSkills(-1);
        assertTrue(Iterables.size(skills) == TOTAL_SKILL_NUMBER);
    }


    private boolean isEqualOrder(Iterable<Skill> oneList, Iterable<Skill> secondList) {
        assertEquals("Size of lists must be equals in order to compare ordering elements",
                Iterables.size(oneList), Iterables.size(secondList));
        int size = Iterables.size(oneList);

        int i = 0;
        for (Skill skillOne : oneList) {
            for (Skill skillSecond : secondList) {
                if (skillOne.equals(skillSecond)) {
                    i++;
                }
            }
        }
        System.out.println("i = " + i);
        return i == size;
    }

}
