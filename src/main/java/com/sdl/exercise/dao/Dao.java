package com.sdl.exercise.dao;

import com.sdl.exercise.model.domain.impl.Priority;

/**
 * SkillDao methods.
 * @param <E>
 */
public interface Dao<E> {
    Iterable<E> listSkills();

    Iterable<E> listSkillsFor(Priority priority);

    Iterable<E> listSkillsGreaterThanOrEqualTo(Priority priority);

}
